#include "uart.h"

volatile int dem=0;
volatile int ktt=0;
volatile int kt=1;
volatile int kt_2=0;
volatile int tmp=0,i=0;
volatile char ar[100];

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;       // CLK_src: DC0 1MHZ

    Configure_IO();
    Configure_Uart();
    Configure_Timer();

    ConfigSever();
    _BIS_SR(GIE);               // enable global interupts

    while(1)
    {

    }
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    ar[i]=UCA0RXBUF;
    i++;
    if (ktt == 1){
    switch (dem)
    {
    case 0: if (UCA0RXBUF != 'L') kt = 0;
            break;
    case 1: if (UCA0RXBUF != 'E') kt = 0;
            break;
    case 2: if (UCA0RXBUF != 'D') kt = 0;
            break;
    case 3: if (UCA0RXBUF != '_') kt = 0;
            break;
    case 4: if (UCA0RXBUF != 'O') kt = 0;
            break;
    case 5: if (UCA0RXBUF == 'N') kt_2 = 1;
            else if (UCA0RXBUF == 'F') tmp = 2;
            else kt = 0;
            break;
    case 6: if (tmp != 2) kt = 0;
    else{
                if (UCA0RXBUF != 'F')
                {
                    kt = 0;
                }
                else kt_2 = 2;
        }
            break;
    default: kt = 0;
            break;

    }

    dem = dem + 1;
    }
    if(UCA0RXBUF == ':') ktt = 1;
    TAR = 0x0000;               // reset time_out
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer_A_1(void)
{

    switch(TA0IV)
    {
    case 2: break;
    case 4: break;
    case 10:
        if (dem!=0)
        {
            if ((dem<6)||((dem == 6) && (kt_2==0))) kt = 0;

            if (kt == 0)
            {
                SendString((unsigned char *)"\r\nAT+CIPSEND=0,6\r\n",strlen("\r\nAT+CIPSEND=0,6\r\n"));
                __delay_cycles(50000);
                SendString((unsigned char *)"NOT\ OK",strlen("NOT\ OK"));
                __delay_cycles(50000);

            }
            else
            {
                SendString((unsigned char *)"\r\nAT+CIPSEND=0,2\r\n",strlen("\r\nAT+CIPSEND=0,2\r\n"));
                __delay_cycles(800000);
                SendString((unsigned char *)"OK",strlen("OK"));
                __delay_cycles(800000);

                if (kt_2 == 1) P1OUT |= BIT0;
                if (kt_2 == 2) P1OUT &= ~BIT0;
            }
        }
        dem = 0;                        // reset biến đếm
        kt = 1;                         // reset biến kiểm tra OK/ Not OK
        kt_2 = 0;                       // reset biến kiểm tra On/ Off
        ktt = 0;
        break;
    default: break;
    }
}
