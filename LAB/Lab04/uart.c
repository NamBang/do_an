/*
 * uart.c
 *
 *  Created on: May 11, 2018
 *      Author: Tran Nam Bang
 */


#include "uart.h"


void Configure_Uart()                   // 1MHZ 9600
{
    P1SEL = BIT1 + BIT2;
    P1SEL2 = BIT1 + BIT2;

    UCA0CTL1 = UCSWRST;
    UCA0CTL0 = 0x00;

        UCA0CTL1 = UCSSEL_2|UCSWRST; // CHON NGUON CLOCK CHO UART LA DOC
        // CHON TOC DO BAUD LA 11250
       /* UCA0MCTL = UCBRS_6;

    UCA0BR0 = 8;
    UCA0BR1 = 00;

        UCA0MCTL = UCBRF_1 |UCBRS_0 | UCOS16;
           UCA0BR0 = 52;
           UCA0BR1 = 00;
           */
        UCA0MCTL = UCBRF_3 |UCBRS_5 | UCOS16;
        UCA0BR0 = 4;
        UCA0BR1 = 0;
    UCA0CTL1 &= ~UCSWRST;
    IE2 |= UCA0RXIE;
}

void Configure_IO(void)                 // LED 1 là output
{
    P1DIR |= BIT0;
    P1OUT &= ~BIT0;
}

void Configure_Timer(void)              // timer dem len, 50ms
{
    TA0CTL |= TASSEL_2 + MC_1 + TAIE;   // timer A clock source: DCO + up mode + enable timer A interupt
    TACCR0 = 50000;

}

// Các Function cho việc gửi chuỗi
void SendByte(unsigned char byte)
{
    while( !(IFG2 & UCA0TXIFG));
    UCA0TXBUF = byte;
}

void SendString(unsigned char *data,unsigned char length )
{
    unsigned int k;
    for(k=0; k < length; k++)
    {
        SendByte(data[k]);
        _delay_cycles(8000);
    }
}

int strlen(char * string){
    unsigned int k = 0;
    while( k < 128 && string[k] != '\0' ) k ++ ;
    return (k < 128)? k : -1 ;
}

void ConfigSever()
{
    SendString((unsigned char *)"AT+CWMODE=3\r\n",strlen("AT+CWMODE=2\r\n"));
   __delay_cycles(800000);
   SendString((unsigned char *)"AT+CWSAP=\"NamBang\",\"\",5,0\r\n",strlen("AT+CWSAP=\"NamBang\",\"\",5,0\r\n"));
   __delay_cycles(800000);
   SendString((unsigned char *)"AT+CIFSR\r\n",strlen("AT+CIFSR\r\n"));
    __delay_cycles(800000);
  SendString((unsigned char *)"AT+CIPMUX=1\r\n",strlen("AT+CIPMUX=1\r\n"));
  __delay_cycles(800000);
  SendString((unsigned char *)"AT+CIPSERVER=1,5000\r\n",strlen("AT+CIPSERVER=1,5000\r\n"));
         __delay_cycles(800000);
}

