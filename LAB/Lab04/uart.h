/*
 * uart.h
 *
 *  Created on: May 11, 2018
 *      Author: Tran Nam Bang
 */

#ifndef UART_H_
#define UART_H_
#include <msp430.h>
#include <msp430g2553.h>

void Configure_IO(void);
void Configure_Uart(void);
void Configure_Timer(void);
void ConfigSever();

#endif /* UART_H_ */
