/*
 * Config_UART.c
 *
 *  Created on: May 17, 2018
 *      Author: Tran Nam Bang
 */
/*
 * uart.c
 *
 *  Created on: May 11, 2018
 *      Author: Tran Nam Bang
 */


#include "Config_UART.h"

void ConfigIO()
{
    P1DIR |= BIT0;
    P1OUT &= ~BIT0;
}

void ConfigClock()
{
    if(CALBC1_8MHZ==0xFF)
    {
        while(1);
    }
    //CAU HINH TAN SO CLOCK LA 8MHZ
    DCOCTL =0;
    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    BCSCTL2 |=SELM_0;//CHON BO CHIA LA 0
}
void ConfigUart()
{
    P1SEL = BIT1 + BIT2;//
    P1SEL2 = BIT1 + BIT2;//Secondary peripheral module function is selected.

    UCA0CTL1 |= UCSWRST;//BAT DAU CAU HINH UART
    UCA0CTL0 = 0X00;

    UCA0CTL1 = UCSSEL_2|UCSWRST; // CHON NGUON CLOCK CHO UART LA DOC
    // CHON TOC DO BAUD LA 9600
    UCA0MCTL = UCBRF_1 |UCBRS_0 | UCOS16;
    UCA0BR0 = 52;
    UCA0BR1 = 00;

    UCA0CTL1 &= ~UCSWRST;// RESET MODULE DE HOAT DONG
    IE2 |= UCA0RXIE;// CHO PHEP NGAT UART RX

    _BIS_SR(GIE);
}
void ConfigTimer()
{
    TA0CTL |= TASSEL_2 + MC_1 + TAIE + ID_3;   // timer A clock source: DCO + up mode + enable timer A interupt+ div 8
    TACCR0 = 50000;

}

void SendByte(unsigned char byte)
{
    while(!(IFG2&UCA0TXIFG));
    UCA0TXBUF = byte;
}

void SendString(unsigned char *data,unsigned char length )
{
    unsigned int k;
    for(k=0;k<length;k++)
    {
        SendByte(data[k]);
        _delay_cycles(8000);
    }
}

int strlen(char * string){
    unsigned int k = 0;
    while( k < 128 && string[k] != '\0' ) k ++ ;
    return (k < 128)? k : -1 ;
}






