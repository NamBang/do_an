#include "Config_UART.h"
// 1 on; 2 OFF
volatile int count=0;
volatile int kt=1;
volatile int O_F=0,temp=0;
int main(void)
{

    WDTCTL = WDTPW | WDTHOLD;
    ConfigIO();
    ConfigClock();
    ConfigUart();
    ConfigTimer();
    SendString((unsigned char *)"NEU GIU chuoi LED_ON  thi bat led va LED_OFF thi tat led\r\n",strlen("NEU GIU chuoi LED_ON  thi bat led va LED_OFF thi tat led\r\n"));

    while(1)
    {
    }
    return 0;
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    switch (count)
    {
    case 0: if (UCA0RXBUF != 'L') kt = 0;
            break;
    case 1: if (UCA0RXBUF != 'E') kt = 0;
            break;
    case 2: if (UCA0RXBUF != 'D') kt = 0;
            break;
    case 3: if (UCA0RXBUF != '_') kt = 0;
            break;
    case 4: if (UCA0RXBUF != 'O') kt = 0;
            break;
    case 5: if (UCA0RXBUF == 'N')  O_F = 1;
            else if (UCA0RXBUF == 'F') temp = 2;
            else kt = 0;
            break;
    case 6: if (UCA0RXBUF == 'F' && temp==2)  O_F = 2;
            else kt=0;
            break;
    default: kt = 0;
            break;

    }

    count = count + 1;
    TAR = 0x0000;
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer_A_1(void)
{
    switch(TA0IV)
    {
    case 2: break;
    case 4: break;
    case 10:
        if (count != 0)
        {
            if (kt == 0)
            {
                SendString((unsigned char *)"\nERROR\n",strlen("\nERROR\n"));
            }
            else
            {
                SendString((unsigned char *)"\nOK\n",strlen("\nOK\n"));
                if ( O_F== 1) P1OUT |= BIT0;
                if ( O_F == 2) P1OUT &= ~BIT0;
            }
        }
        count = 0;
        kt = 1;
        O_F = 0;
        break;
    default: break;
    }
}
