/*
 * Config_UART.h
 *
 *  Created on: May 17, 2018
 *      Author: Tran Nam Bang
 */

#ifndef CONFIG_UART_H_
#define CONFIG_UART_H_
#include <msp430.h>

void ConfigIO();
void ConfigClock();
void ConfigUart();
void ConfigTimer();
void SendByte(unsigned char byte);
int strlen(char * string);
void SendString(unsigned char *data,unsigned char length);

#endif /* CONFIG_UART_H_ */
