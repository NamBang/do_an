/*
 * functionc.h
 *
 *  Created on: Apr 11, 2018
 *      Author: Tran Nam Bang
 */


#ifndef FUNCTIONC_H_
#define FUNCTIONC_H_
#include <msp430.h>
#include <math.h>
void ConfigIO();
void ConfigClock();
void ConfigUart();
void SendByte(unsigned char byte);
int strlen(char * string);
void SendString(unsigned char *data,unsigned char length);
void findRssi();
unsigned char uart_getc();
void uart_gets(void);
void calculateDistance();
void toado();
void UART_Write_Char(unsigned char data);       //Gui ki tu
void UART_Write_Int(unsigned long n);           //Goi so kieu int
        //In ra gia tri bit cua thanh ghi
void UART_Write_Float(float x,unsigned char coma);      //coma<=4

#endif /* FUNCTIONC_H_ */
