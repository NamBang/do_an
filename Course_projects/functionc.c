#include "functionc.h"

volatile float x;
volatile float y;
volatile  int rssi[3];
volatile float r[2];
volatile  unsigned char arr[20];
volatile float i1,j1;
volatile int td[6];

void UART_Write_Char(unsigned char data)
{
    while (!(IFG2&UCA0TXIFG)); // Doi gui xong ky tu truoc
    UCA0TXBUF= data; // Moi cho phep gui ky tu tiep theo
}
void UART_Write_Int(unsigned long n)
{
     unsigned char buffer[16];
     unsigned char i=0,j=0;
     if(n == 0) {
         UART_Write_Char('0');
          return;
     }
     for (i = 15; i > 0 && n > 0; i--) {
          buffer[i] = (n%10)+'0';
          n /= 10;
     }

     for(j = i+1; j <= 15; j++) {
         UART_Write_Char(buffer[j]);
     }
}
void ConfigIO()
{
    P1DIR |= BIT1+BIT6;
    P1OUT =0xff;
}
void ConfigClock()
{
    if(CALBC1_8MHZ==0xFF)
    {
        while(1);
    }
    //CAU HINH TAN SO CLOCK LA 8MHZ
    DCOCTL =0;
    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    BCSCTL2 |= SELM_0;//CHON BO CHIA LA 0
}
void ConfigUart()
{
    P1SEL = BIT1 + BIT2;//
    P1SEL2 = BIT1 + BIT2;//Secondary peripheral module function is selected.
    UCA0CTL1 |= UCSWRST;//BAT DAU CAU HINH UART
    UCA0CTL0 = 0X00;
    UCA0CTL1 = UCSSEL_2|UCSWRST; // CHON NGUON CLOCK CHO UART LA DOC
    // CHON TOC DO BAUD LA 11250
    UCA0MCTL = UCBRF_3 |UCBRS_5 | UCOS16;
    UCA0BR0 = 4;
    UCA0BR1 = 0;
    /*
    UCA0MCTL = UCBRF_1 |UCBRS_0 | UCOS16;
    UCA0BR0 = 52;
    UCA0BR1 = 00;*/
    UCA0CTL1 &= ~UCSWRST;// RESET MODULE DE HOAT DONG
    IE2 |= UCA0RXIE;// CHO PHEP NGAT UART RX
    _BIS_SR(GIE);//CHO PHEPNGAT TOAN CUC
}
void SendByte(unsigned char byte)
{
    while(!(IFG2&UCA0TXIFG));
    UCA0TXBUF = byte;
}
void SendString(unsigned char *data,unsigned char length )
{
    unsigned int k;
    for(k=0;k<length;k++)
   {
        SendByte(data[k]);
        _delay_cycles(8000);
    }
}
int strlen(char * string){
    unsigned int k = 0;
    while( k < 128 && string[k] != '\0' ) k ++ ;
    return (k < 128)? k : -1 ;
}
void findRssi()
{
     unsigned char i=0,j=0;
    for(i=i+2;i<20;i++)
    {
        if(arr[i]=='(')
        {
            if(arr[i+3]==')')
            {
                int temp= (arr[i+1]-48)*10+(arr[i+2]-48);
                rssi[j]=temp;
                j++;
            }
            else
            {
                int temp= (arr[i+1]-48)*100 + (arr[i+2]-48)*10 + (arr[i+3]-48);
                rssi[j]=temp;
                j++;
            }
        }
        else
            continue;
    }
}
void calculateDistance()
{

    unsigned char  j=0;
   unsigned char     i=0;
    for(i=0;i<3;i++)
    {
       float exp = -(float)(19.5-rssi[i])/(10.0*6);
        r[j] = (float)pow(10.0,exp);
        j++;
        exp=0.0;

    }
}

void toado()
{
     float xt,yt,cosa,d;

     int x0,y0,x1,y1,x2,y2;
     // don vi cm

     x0= td[0];
     y0= td[1];
     x1= td[2];
     y1= td[3];
     x2= td[4];
     y2= td[5];

     float a,b,temp;
     int  Dy;
     int Dx,D;
     D=x0-x1;
     Dx=y0-y1;
     Dy=(x0/10)*(y1/10)-(x1/10)*(y0/10);
     a=(float)Dx/D;
     b=((float)Dy/D)*100;
     j1= 100*abs(((float)(a/10))*((float)(x2/10))-((float)y2/100)+((float)(b/100)))/(sqrt(a*a+1));
     temp= ((float)(x2-x0)/10)*((float)(x2-x0)/10)+((float)(y2-y0)/10)*((float)(y2-y0)/10);
     i1=sqrt(temp-(j1/10.0)*(j1/10.0))*10;
     d =sqrt( ((float)(x1-x0)/10)*((float)(x1-x0)/10) +  ((float)(y1-y0)/10)* ((float)(y1-y0)/10) )*10;
     xt = ((r[0]*r[0]) - (r[1]*r[1])+ d*d)/(2*d);
     yt = (r[0]*r[0]-r[2]*r[2]+i1*i1+j1*j1)/(2*j1) - (i1*xt)/j1;
    cosa=-((float)(x1-x0)/d);  //cos=canh/huyenh
    float sina1=((float)(y1-y0)/d);
     x=x0-xt*sina1+cosa*yt;
     y=y0-xt*cosa-yt*sina1;
    // x=x0-x3;
  //  y=y0-y3;
     // don vi met

     /*

      // don vi met////////////////////////////////////////////////////////////////////////////
     float x0,y0,x1,y1,x2,y2;
      x0= (float)td[0]/100.0;
      y0= (float)td[1]/100.0;
      x1= (float)td[2]/100.0;
      y1= (float)td[3]/100.0;
      x2= (float)td[4]/100.0;
      y2= (float)td[5]/100.0;

      float a,b,temp;
      int  Dy;
      int Dx,D;
      D=x0-x1;
      Dx=y0-y1;
      Dy=(x0)*(y1)-(x1)*(y0);
      a=(float)Dx/D;
      b=((float)Dy/D);
      j1= (float)abs(a*x2-y2+b)/(sqrt(a*a+1));
      temp= (x2-x0)*(x2-x0)+(y2-y0)*(y2-y0);
      i1=sqrt(temp-j1*j1);
      d =sqrt( (x1-x0)*(x1-x0) +  (y1-y0)* (y1-y0));
      xt = ((r[0]*r[0]) - (r[1]*r[1])+ d*d)/(2*d);
      float yt1 = (r[0]*r[0]-r[2]*r[2]);
      float yt2=i1*i1+j1*j1;
      float yt3= -(i1*xt)/j1;
      yt=(yt1+yt2)/(2*j1)+yt3;

     cosa=-((float)(x1-x0)/d);  //cos=canh/huyenh
     float sina1=((float)(y1-y0)/d);
      x=x0-xt*sina1+cosa*yt;
      y=y0-xt*cosa-yt*sina1;
      */
}


